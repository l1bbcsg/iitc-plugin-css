// ==UserScript==
// @id             iitc-plugin-css
// @name           IITC plugin: Custom CSS
// @author         @l1bbcsg
// @category       Tweaks
// @version        1.0.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/l1bbcsg/iitc-plugin-css/raw/master/iitc-plugin-css.user.js
// @description    Adda ability to add custom CSS styles
// @match          *://ingress.com/intel
// @match          *://www.ingress.com/intel
// @match          *://intel.ingress.com/*
// @include        /^https?\:\/\/(www\.)?ingress.com\/intel$/
// @include        /^https?\:\/\/intel.ingress.com/
// @grant          none
// ==/UserScript==

(function() {
	const NAME = 'iitc-plugin-css';
	const VERSION = '1.0.0.20220204';

	const LS_KEY = 'iitc-plugin-custom-css';

	if (typeof window.plugin !== 'function') {
		window.plugin = function() {};
	}

	window.plugin.customsCss = function() {};

	window.plugin.customsCss.showDialog = () => {
		const existing = document.getElementById('custom-css-plugin');

		const html = `
			<div> 
				<textarea id="custom-css-input" style="width:100%">${existing?.innerHTML ?? ''}</textarea>
			</div>
		`;

		const closeCallback = () => {
			const content = document.getElementById('custom-css-input')?.value ?? null;
			window.plugin.customsCss.applyStyle(content);

			if (content) {
				localStorage.setItem(LS_KEY, content);
			} else {
				localStorage.removeItem(LS_KEY);
			}
		}

		dialog({
			html,
			title: 'Custom CSS',
			modal: true,
			id: 'custom-css',
			closeCallback,
		})
	}

	window.plugin.customsCss.setup = () => {
		const buttonElement = document.createElement('a');
		buttonElement.innerHTML = 'Custom CSS';
		buttonElement.setAttribute('id', 'show-css-dialog');
		buttonElement.addEventListener('click', (event) => {
			window.plugin.customsCss.showDialog();
			event.preventDefault();
		});

		document.getElementById('toolbox').appendChild(buttonElement);

		const stored = localStorage.getItem(LS_KEY);
		if (stored) {
			window.plugin.customsCss.applyStyle(stored);
		}
	}

	window.plugin.customsCss.applyStyle = (style) => {
		const id = 'custom-css-plugin';

		const existing = document.getElementById(id);
		if (existing) {
			if (style) {
				existing.innerHTML = style;
			} else {
				existing.parentElement.removeChild(existing);
			}
		} else if (style) {
			const stylesheet = document.createElement('style');
			stylesheet.setAttribute('id', id);
			stylesheet.innerHTML = style;
			document.head.appendChild(stylesheet)
		}
	}

	const setup = window.plugin.customsCss.setup;

	setup.info = {
		buildName: NAME + ':' + VERSION,
		dateTimeVersion: VERSION,
		pluginId: NAME,
	};

	window.bootPlugins = window.bootPlugins ?? [];
	window.bootPlugins.push(setup);

	if (window.iitcLoaded && typeof setup === 'function') {
		setup();
	}
})();
